package ee.yals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Stub test class
 *
 * @since 2.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class YalsApplicationTests {

	@Test
	public void contextLoads() {
	}

}
